"use client"

import React from 'react';
import Link from 'next/link'
import { FaGitlab , FaLinkedin , FaFacebook , FaTelegram} from "react-icons/fa"


const socials =[
    {
        icon: <FaGitlab/>,
        path: "",
    },
    {
        icon: <FaLinkedin/>,
        path: "",
    },
    {
        icon: <FaFacebook/>,
        path: "",
    },
    {
        icon: <FaTelegram/>,
        path: "",
    }
]

function Socials({containerStyle , iconStyle,}) {
    return (
        <div className={containerStyle}>
            {
                socials.map((social, index) => {
                    return (
                     <Link key={index} href={social.path} className={iconStyle} >{social.icon}</Link>
                    )
                })
            }
        </div>
    );
}

export default Socials;
