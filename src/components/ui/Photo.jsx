"use client"

import React from 'react';
import {motion} from "framer-motion"
import Image from "next/image"


function Photo() {
    return (
        <div className={"w-full h-full relative flex justify-center "}>
            <motion.div
                initial={{opacity: 0}} animate={{
                opacity: 1, transition: {
                    delay: 2,
                    duration: 0.4,
                    ease: "easeIn",
                }
            }}>
                <motion.div
                    initial={{opacity: 0}}
                    animate={{
                        opacity: 1,
                        transition: {
                            delay: 2.4,
                            duration: 0.4,
                            ease: "easeInOut",
                        }
                    }}
                    className={" w-[348px] h-[248px] xl:w-[298px] xl:h-[298px] absolute xl:top-20 xl:left-[100px]"}>
                    <Image src={"/assets/Chhuonmakararoth.png"} priority quality={100} fill alt={"Chhuon MakaraRoth"}
                           className={"object-contain xl:w-[50%] xl:h-[50%] rounded-full"}/>
                </motion.div>
            </motion.div>
            <motion.svg
                fill={"transparent"}
                viewBox={"0 0 506 506"}
                xmlns={"http://www.w3.org/2000/sv"}
                className={"w-[350px] xl:w-[506px] h-[350px] xl:h-[506px]"}>
                <motion.circle
                    cx={"253"}
                    cy={"253"}
                    r={"250"}
                    stroke={"#00ff99"}
                    strokeWidth={"4"}
                    strokeLinecap={"round"}
                    strokeLinejoin={"round"}
                    initial={{strokeDasharray: "24 10 0 0 "}}
                    animate={{
                        strokeDasharray: ["15 120 25 25 ", "16 25 92 72 ", " 4 250 22 22 "],
                        rotate: [120, 360],
                        transition: {
                            opacity: 1,
                            duration: 20,
                            repeat: Infinity,
                            repeatType: "reverse",
                        }
                    }}
                />
            </motion.svg>
        </div>
    );
}
export default Photo;
