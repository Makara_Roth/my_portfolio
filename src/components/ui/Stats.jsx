"use client"

import React from 'react';
import  CountUp from 'react-countup'

const stats= [
    {
        num : 1,
        text : 'Year Experience'
    },
    {
        num : 5,
        text : 'Projects Completed'
    },
    {
        num : 10,
        text : 'Technology Mastered'
    },
    {
        num : 300,
        text : 'Code commited'
    },
]


const Stats = () => {
    return (
      <section className={"pt-4 pb-12 xl:pt-0 xl:pb-0 flex justify-center"}>
          <div className={"container mx-auto"}>
              <div className={"flex flex-wrap gap-6 max-w-[80vw] mx-auto xl:max-w-none"}>
                  {
                      stats.map((items , index) => {
                          return (
                              <div key={index} className={"flex-1  flex justify-center xl:justify-start items-center gap-4"}>
                                  <CountUp start={0} end={items.num} duration={5} delay={2} className={"text-2xl xl:text-6xl font-extrabold"} separator=","/>
                                  <p className={`${items.text.length < 15 ? "max-w-[100px]"  : " max-w-[150px]"} leading-snug text-white/80`}>{items.text}</p>
                              </div>
                          )
                      })
                  }
              </div>
          </div>
      </section>
    );
}

export default Stats;
