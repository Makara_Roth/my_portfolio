"use client"

import React from 'react'
import { usePathname } from 'next/navigation'
import Link from 'next/link'

const links =[
    {
        name : "Home ",
        path: "/",
    },
    {
        name : "Service ",
        path: "/service",
    },
    {
        name : "Resume",
        path: "/resume",
    },
    {
        name : "Work",
        path: "/work",
    },
    {
        name : "Contact",
        path: "/contact",
    }
]
const Nav = () => {
const  pathname=usePathname()
  return (
    <nav className='flex gap-8'>
    {
        links.map((item , index) =>{
            return <Link href={item.path} key={index} 
            className={`${item.path === pathname && "text-accent border-accent "} capitalize font-medium hover:text-accent`}>{item.name}</Link>
        })
    }
    </nav>
  )
}

export default Nav