import React from 'react';
import Link from 'next/link'
import {Button} from './ui/button'
import Nav from './ui/Nav';
import MobileNav from "@/components/ui/MobileNav";

const Header = () => {
  return (
    <header className="py-8 xl:py-12 text-white">
        <div className="container mx-auto flex justify-between items-center ">
            <Link href={"/"}>
                <h1>MakaraRoth <span> . </span></h1>
            </Link>
            {/* nav bar */}
           <div className="hidden xl:flex items-center  gap-8">
           <Nav/>
           <Link href={"/contact"}>
            <Button> Hire Me</Button>
           </Link>
           </div>
            {/* Mobile nav */}
            <div className={"xl:hidden"}>
                <MobileNav/>

            </div>
        </div>
    </header>
  )
}

export default Header
