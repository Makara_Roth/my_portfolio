"use client";

import { AnimatePresence, motion } from "framer-motion";
import { usePathname } from "next/navigation";
import Stairs from "../components/Stairs";

function StairTransition() {
  const pathname = usePathname();
  return (
    <>
      <AnimatePresence mode="wait">
        <div key={pathname}>
          <div
            className={
              "h-screen w-screen  fixed left-0 top-0 " +
              "right-0 pointer-events-none z-40 flex "
            }
          >
            <Stairs />
          </div>
          <motion.div
            initial={{ opacity: 1 }}
            animate={{
              opacity: 0,
              transition: {
                delay: 1,
                duration: 0.4,
                ease: "easeInOut",
              },
            }}
            className={
              "h-screen w-screen bg-primary fixed top-0 pointer-events-none"
            }
          />
        </div>
      </AnimatePresence>
    </>
  );
}
export default StairTransition;
