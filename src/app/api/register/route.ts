import connect from "@/lib/db";
import User from "@/lib/models/user";

import bcript from "bcryptjs"
import { NextResponse } from "next/server";

export const POST = async (request: Request) => {
  try {
    const { user_name, email, password } = await request.json();
    const hashPassword = await bcript.hash(password, 15);
    await connect();
    const userExists = await User.findOne({ email }).select("_id");
    if(!userExists){
      const user = new User({ user_name, email, password: hashPassword })
      await user.save();
      return new NextResponse(
        JSON.stringify({ Message: " User Cretaed", data: user }),
        { status: 200 },
      );
    }else{
      return new NextResponse(
        JSON.stringify({ Message: " User Exists"}),
        { status: 400 },
      );
    }
  } catch (error) {
    return new NextResponse(JSON.stringify({ error: error.message }));
  }
};