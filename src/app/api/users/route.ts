import connect from "@/lib/db";
import User from "@/lib/models/user";
import { Types } from "mongoose";
import bcript from "bcryptjs"
import { NextResponse } from "next/server";

const ObjectId = require("mongoose").Types.ObjectId;

export const GET = async () => {
  try {
    await connect();
    const users: any = await User.find();

    return new NextResponse(
      JSON.stringify({ Message: "suessage", data: users }),
      { status: 200 },
    );
  } catch (err) {
    return new NextResponse("Server Error" + err.messgae, { status: 500 });
  }
};

export const PATCH = async (req: Request) => {
  try {
    const body = await req.json();
    const { userId, newUserName } = body;
    await connect();
    if (!userId || !newUserName) {
      return new NextResponse(
        JSON.stringify({ message: "User not Found !!!" }),
        { status: 400 },
      );
    }
    if (!Types.ObjectId.isValid(userId)) {
      return new NextResponse(
        JSON.stringify({ message: "User not Found !!!" }),
        { status: 400 },
      );
    }
    const updatUser = await User.findOneAndUpdate(
      { _id: new ObjectId(userId) },
      { user_name: newUserName },
      { new: true },
    );
    return new NextResponse(
      JSON.stringify({ Message: " User Updated", data: updatUser }),
      { status: 200 },
    );
  } catch (error) {
    return new NextResponse(JSON.stringify({ error: error.message }));
  }
};

export const DELETE = async (req: Request) => {
  try {
    const { searchParams } = new URL(req.url);
    const userId = searchParams.get("userId");
    await connect();

    if (!Types.ObjectId.isValid(userId)) {
      return new NextResponse(
        JSON.stringify({ message: "User not Found !!!" }),
        { status: 400 },
      );
    }
    const deleteUser = await User.findOneAndDelete(new Types.ObjectId(userId));
    if (!deleteUser) {
      return new NextResponse(
        JSON.stringify({ message: "Delete not complete" }),
        { status: 400 },
      );
    }
    return new NextResponse(JSON.stringify({ Message: " User Deleted" }), {
      status: 200,
    });
  } catch (error) {
    return new NextResponse(JSON.stringify({ error: error.message }));
  }
};
