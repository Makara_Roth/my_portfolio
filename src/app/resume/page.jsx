"use Client";
import {motion} from 'framer-motion'
import {FaCss3, FaFigma, FaHtml5, FaJs, FaNodeJs, FaReact} from "react-icons/fa";
import {SiNextdotjs, SiTailwindcss} from "react-icons/si";
import {Tabs, TabsContent, TabsList, TabsTrigger} from '@/components/ui/tabs'
import {Tooltip, TooltipContent, TooltipProvider, TooltipTrigger} from '@/components/ui/tooltip'
import {ScrollArea} from '@/components/ui/scroll-area'

//info data
const about = {
    title: "About Me",
    description: "I am currently a sophomore in year 4 at the Royal University of Phnom Penh (RUPP), majoring in the Department of Information Technology. I'm very interested in doing an internship as a Web developer. Highly impressed with learning new things.",
    info: [
        {
            fileName: "Name",
            fieldValue: "Chhuon MakaraRoth"
        },
        {
            fileName: "Phone",
            fieldValue: "(+855) 081 693 071"
        },
        {
            fileName: "Experience",
            fieldValue: "1year"
        },
        {
            fileName: "Linkedin",
            fieldValue: "Chhuon MakaraRoth"
        },
        {
            fileName: "Nationality",
            fieldValue: "Cambodian"
        },
        {
            fileName: "Email",
            fieldValue: "chhuonmakararoth@gmail.com"
        },
        {
            fileName: "Telegram",
            fieldValue: "(+855) 081 693 071"
        },
        {
            fileName: "language",
            fieldValue: "Khmer , English"
        },
    ]
}

// experience
const experience = {
    icon: "/assets/resume/badge.svg",
    title: "My experience",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dis consectetur eros mi. In ac dui non nun.",
    items: [
        {
            company: " KIloIT",
            position: "Front End Developer (React js ) Training",
            duration: "2022 - 2023",
        },
        {
            company: "Sea Games 2022",
            position: "Game IT ",
            duration: "2023",
        },
        {
            company: "Para Games 2022",
            position: "Game IT",
            duration: "2023",
        },
        {
            company: " ACM INDOCHINA CAMBODIA",
            position: "Web Developer",
            duration: "2020 - 2021",
        },
    ]
}

// education
const education = {
    icon: "/assets/resume/cap.svg",
    title: "My Education",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dis consectetur eros mi. In ac dui non nun.",
    items: [
        {
            institution: "Royal University of Phnom Penh",
            degree: "Bachelor Degree",
            duration: "2021 - 2025",
        },
        // {
        //     institution: " KIloIT",
        //     degree: "Front End Developer intern ",
        //     duration: "2020 - 2021",
        // },
        // {
        //     institution: " KIloIT",
        //     degree: "Front End Developer intern ",
        //     duration: "2020 - 2021",
        // },
    ]
}

// skills
const skills = {
    title: "My skills",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dis consectetur eros mi. In ac dui non nun.",
    skillLists: [
        {
            icon: <FaHtml5/>,
            name: "HTML5"
        },
        {
            icon: <FaCss3/>,
            name: "Css 3"
        },
        {
            icon: <FaJs/>,
            name: "JavaScript"
        },
        {
            icon: <FaReact/>,
            name: "React js"
        },
        {
            icon: <SiNextdotjs/>,
            name: "Next.js"
        },
        {
            icon: <SiTailwindcss/>,
            name: "Tailwind.css"
        },
        {
            icon: <FaNodeJs/>,
            name: "Node.js"
        },
        {
            icon: <FaFigma/>,
            name: "Figma"
        },
    ]
}

// projects

export default function Resume () {

    return (
        <>
            <motion
                initial={{opacity: 0}}
                animate={{
                    opacity: 1,
                    transition: {
                        delay: 2.4,
                        duration: 0.4,
                        ease: "easeIn"
                    }
                }}
                className={"min-h-[80vh] flex items-center justify-center py-12 xl:py-0"}
            >
                <div className={"container mx-auto"} >
                    <Tabs defaultValue={"experience"} className={"flex flex-col xl:flex-row gap-[60px]"}>
                        <TabsList className={"flex flex-col w-full max-w-[380px] mx-auto xl:mx-0 " +
                            "gap-6 "}>
                            <TabsTrigger value={"experience"}>Experience</TabsTrigger>
                            <TabsTrigger value={"education"}>Education</TabsTrigger>
                            <TabsTrigger value={"skills"}>Skills</TabsTrigger>
                            <TabsTrigger value={"about"}>About</TabsTrigger>
                            {/*<TabsTrigger>Projects</TabsTrigger>*/}
                        </TabsList>
                        <div className={"min-h-[70vh] w-full "}>
                            <TabsContent value={"experience"} className={"w-full"}>
                               <div className={"flex flex-col gap=[30px] text-center xl:text-left "}>
                                   <h3 className={"text-4xl font-bold "}>{experience.title}</h3>
                                   <p className={"max-w-[600px] text-white-[60px] mx-auto xl:mx-0"}>{experience.description}</p>
                                <ScrollArea className={"h-[400px]"}>
                                    <ul className={"grid grid-cols-1 lg:grid-cols-2  gap-[30px]"}>
                                        {
                                            experience.items.map((items, index) => {
                                                return (
                                                    <li key={index} className={"bg-[#232329] h-[184px] py-6  px-10 rounded-xl flex flex-col justify-center items-center lg:items-start gap-1"}>
                                                        <span className={"text-accent"}>{items.duration}</span>
                                                        <h3 className={"text-xl max-w-[260px] min-h-[60px] text-center lg:text-left"}>{items.position}</h3>
                                                        <div className={"flex items-center gap-3"}>
                                                            <span className={"w-[6px] h-[6px] rounded-full bg-accent"}></span>
                                                            <p className={"text-white/60"}>{items.company}</p>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul>
                                </ScrollArea>
                               </div>
                            </TabsContent>

                            <TabsContent value={"education"} className={"w-full"}>
                                <div className={"flex flex-col gap=[30px] text-center xl:text-left "}>
                                    <h3 className={"text-4xl font-bold "}>{education.title}</h3>
                                    <p className={"max-w-[600px] text-white-[60px] mx-auto xl:mx-0"}>{education.description}</p>
                                    <ScrollArea className={"h-[400px]"}>
                                        <ul className={"grid grid-cols-1 lg:grid-cols-2  gap-[30px]"}>
                                            {
                                                education.items.map((items, index) => {
                                                    return (
                                                        <li key={index}
                                                            className={"bg-[#232329] h-[184px] py-6  px-10 rounded-xl flex flex-col justify-center items-center lg:items-start gap-1"}>
                                                            <span className={"text-accent"}>{items.duration}</span>
                                                            <h3 className={"text-xl max-w-[260px] min-h-[60px] text-center lg:text-left"}>{items.degree}</h3>
                                                            <div className={"flex items-center gap-3"}>
                                                                <span
                                                                    className={"w-[6px] h-[6px] rounded-full bg-accent"}></span>
                                                                <p className={"text-white/60"}>{items.institution}</p>
                                                            </div>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    </ScrollArea>
                                </div>
                            </TabsContent>
                            <TabsContent value={"skills"} className={"w-full h-full"}>
                                <div className={"flex flex-col gap-[30px] "}>
                                    <div className={"flex flex-col gap=[30px] text-center xl:text-left "}>
                                        <h3 className={"text-4xl font-bold"}>{skills.title}</h3>
                                        <p className={"max-w-[600px] text-white/60 mx-auto xl:mx-0"}>{skills.description}</p>
                                    </div>
                                </div>
                                <ul className={"grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 xl:gap=[30px] gap-4 mt-4"}>
                                    {skills.skillLists.map((skill , index )=>{
                                        return ( <li key={index}>
                                            <TooltipProvider delayDuration={100}>
                                                <Tooltip>
                                                    <TooltipTrigger classname={"w-full bg-[#232329] py-6  px-10  h-[150px] rounded-xl flex justify-center items-center group"}>
                                                        <div className={"text-6xl group-hover:text-accent transition-all duration-500"} >{skill.icon }</div>
                                                    </TooltipTrigger>
                                                    <TooltipContent>
                                                        <p className={"capitalize"}>{skill.name}</p>
                                                    </TooltipContent>
                                                </Tooltip>
                                            </TooltipProvider>
                                        </li>)
                                    })}
                                </ul>
                            </TabsContent>
                            <TabsContent value={"about"} className={"w-full text-center xl:text-left"}>
                                <div className={"flex flex-col gap-[30px] "}>
                                    <h3 className={"text-xl font-bold"}>{about.title}</h3>
                                    <p className={"max-w-[600px] text-white/60 mx-auto xl:mx-0"}>{about.description}</p>
                                    <ul className={"grid grid-cols-1 lg:grid-cols-2 gap-y-6 max-w-[620px ] mx-auto xl:mx-0 "}>
                                        {
                                            about.info.map((item, index) => {
                                            return (
                                                <li key={index} className={"flex items-center gap-3 justify-center xl:justify-start gap-4"}>
                                                    <span className={"text-white/60"}>{item.fileName}</span>
                                                    <span className={"text-xl"}>{item.fieldValue}</span>
                                                </li>
                                            )
                                        })
                                        }
                                    </ul>
                                </div>
                            </TabsContent>
                        </div>
                    </Tabs>
                </div>
            </motion>
        </>

    )
}

