import { Button } from "../components/ui/button";
import { FiDownload } from "react-icons/fi";

// components
import Socials from "../components/ui/Socials";
import Photo from "../components/ui/Photo";
import Stats from "../components/ui/Stats";

export default function Home() {
  return (
    <section className={"h-full"}>
      <div className={"container mx-auto h-full "}>
        <div
          className={
            "flex flex-col xl:flex-row item-center justify-between xl:pt-8 xl:pb-24"
          }
        >
          <div className={" text-center xl:text-left order-2 xl:order-none "}>
            <span className={"text-xl"}> Web developer</span>
            <h1 className={"h1"}>
              Hello I am <br />{" "}
              <span className={"text-accent"}>Makara Roth</span>
            </h1>
            <p className={" mb-9 text-white/80"}>
              I am a full stack web developer with a passion for designing and
              building beautiful user experiences.
            </p>
            <div
              className={
                " flex flex-col xl:flex xl:flex-row items-center gap-8"
              }
            >
              <Button
                variant={"outline"}
                size={"lg"}
                className={"uppercase flex items-center gap-2"}
              >
                {" "}
                <span> Download CV </span>
                <FiDownload className={"text-xl"} />
              </Button>
              <div className={" mb-8 xl:mb-0"}>
                <Socials
                  containerStyle={"flex gap-6"}
                  iconStyle={
                    "w-9 h-9 border border-accent rounded-full flex justify-center items-center " +
                    "text-accent text-base hover:bg-accent hover:text-primary hover:transition-all duration-500"
                  }
                />
              </div>
            </div>
          </div>
          <div className={" order-1 xl:order-none mb-8 xl:mb-0"}>
            <Photo />
          </div>
        </div>
      </div>
      <Stats />
    </section>
  );
}
