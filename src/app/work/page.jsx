"use client"

import { motion } from "framer-motion"
import "swiper/css"
import { useState } from "react";
import Link from 'next/link'
import {Tooltip, TooltipContent, TooltipProvider, TooltipTrigger} from "@/components/ui/tooltip";
import {BsArrowUpRight, BsGitlab} from "react-icons/bs";
import {SwiperSlide , Swiper } from "swiper/react";
import Image from "next/image";
import WorksSliderButton from "@/components/ui/WorksSliderButton";

const projects = [
    {
        num: '01',
        category: "frontend",
        title: "Restaurant",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non quam lectus. Sed in libero id lectus pulvinar ullamcorper.",
        stacks: [
            { name: "HTML5" },
            { name: "CSS3" },
            { name: "React js" },
            { name: "JavaScript" },
            { name: "Bootstrap" },
            { name: "React js" },
            { name: "Redux" },
            { name: "Redux ToolKit" },
            { name: "Axios" },
            { name: "Material UI" },
        ],
        images:'/assets/work/free-dashboard-templates.jpg',
        live: '',
        gitLub: '',
    },
    {
        num: '02',
        category: "frontend",
        title: "Restaurant",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non quam lectus. Sed in libero id lectus pulvinar ullamcorper.",
        stacks: [
            { name: "HTML5" },
            { name: "CSS3" },
            { name: "React js" },
            { name: "JavaScript" },
            { name: "Bootstrap" },
            { name: "React js" },
            { name: "Redux" },
            { name: "Redux ToolKit" },
            { name: "Axios" },
            { name: "Material UI" },
        ],
        images:'/assets/work/free-dashboard-templates.jpg',
        live: '',
        gitLub: '',
    },
]

const Works = () => {
    const [project, setProject] = useState(projects[0]);
    const handleSlideChange =  (swiper)  =>{
     const currentProject =  swiper.activeIndex;
     setProject(projects[currentProject]);
    }

    return (
        <motion.section initial={{ opacity: 0 }} animate={{ opacity: 1  , transition: {delay: 2.4 , duration : 0.4 , ease : "easeIn"}}} className={"min-h-[80hv] flex flex-col justify-center py-12  xl:px-0"}>
            <div className={"container mx-auto"}>
                <div className={"flex flex-col xl:flex-row xl:gap-[30px]"}>
                    <div className={"w-full xl:w-[50%] xl:h-[460px] flex flex-col xl:justify-between order-2 xl:order-none"}>
                        <div className={"flex flex-col gap-[30px] h-[50%]"}>
                            <div className={"text-8xl leading-none font-extrabold text-transparent text-outline"}>
                                {project.num}
                            </div>
                            <h2 className={"text-[42px] font-bold leading-none text-white group-hover:text-accent transition-all duration-500 capitalize"}>
                                {project.category} Project
                            </h2>
                            <p className={"text-white/60"}>
                                {project.description}
                            </p>
                            <ul className={"flex flex-wrap gap-4 "}>
                                {project.stacks.map((item, index) => (
                                    <li key={index} className={" text-xl text-accent "}>
                                        {item.name}
                                        {index !== project.stacks.length - 1 && ","}
                                    </li>
                                ))}
                            </ul>
                            <div className={" border-t-4 border-white/20"}>
                                <div className={"flex items-center gap-4 mt-2"}>
                                    <Link href={project.live}>
                                        <TooltipProvider delayDuration={100}>
                                            <Tooltip>
                                                <TooltipTrigger className={"w-[60px] h-[60px] rounded-full bg-white/5 flex justify-center items-center group"}>
                                                        <BsArrowUpRight className={"text-3xl group-hover:text-accent"}/>
                                                </TooltipTrigger>
                                                <TooltipContent>
                                                    <p>GitLab</p>
                                                </TooltipContent>
                                            </Tooltip>
                                        </TooltipProvider>
                                    </Link>
                                    <Link href={project.gitLub}>
                                        <TooltipProvider delayDuration={100}>
                                            <Tooltip>
                                                <TooltipTrigger className={"w-[60px] h-[60px] rounded-full bg-white/5 flex justify-center items-center group"}>
                                                    <BsGitlab className={"text-3xl group-hover:text-accent"}/>
                                                </TooltipTrigger>
                                                <TooltipContent>
                                                    <p>GitLab</p>
                                                </TooltipContent>
                                            </Tooltip>
                                        </TooltipProvider>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={"w-full xl:w-[50%]"}>
                        <Swiper
                            spaceBetween={30} slidesPerView={1} className={"xl:h-[520px] mb-12  "} onSlideChange={handleSlideChange}>
                            {
                                projects.map((swiper, index) =>{
                                    return <SwiperSlide key={index}>
                                        <div className={"absolute top-0 bottom-0 w-full h-full bg-black/10 "}>

                                        </div>
                                    <div className={"h-[460px] relative group flex justify-center items-center bg-pink-50/20 "}>
                                        <div className={"relative w-full h-full"}>
                                            {project.images}
                                          <Image src={project.images} fill className={"object-cover"}  alt={"Hello"} />
                                        </div>
                                    </div>
                                    </SwiperSlide>
                                })
                            }
                            <WorksSliderButton 
                            containerStyle="flex gap-2  absolute right-0 bottom-[calc(50%_-_22px)] xl:bottom-0 z-20 w-full justify-between xl:w-max xl:justify-node" 
                            btnStyle="bg-accent hover:bg-accent-hover text-primary text-[22px] w-[44px] h-[44px] flex justify-center items-center transition-all" 
                            />
                        </Swiper>
                    </div>
                </div>
            </div>
        </motion.section>
    )
}

export default Works
