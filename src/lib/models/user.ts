import { Schema, models, model } from "mongoose";

const userScheme = new Schema(
  {
    user_name: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, require: true },
  },
  {
    timestamps: true,
    toJSON: {
         transform: function (doc, ret) {
           delete ret.password; // Remove password field when converting to JSON
           return ret;
         },
       },
    
  },
);
const User = models.User || model("User", userScheme);

export default User;
