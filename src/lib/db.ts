import mongoose from "mongoose";

const uri = process.env.MONGODB_URI;

const connect = async () => {
  const db = mongoose.connection.readyState;

  if (db == 1) {
    console.log("Database Connected ");
    return;
  } else {
    console.log("Database Connecting .........");
  }
  try {
    mongoose.connect(uri!, {
      dbName: "Cluster0",
      bufferCommands: true,
    });
    console.log("Connected");
  } catch (error) {
    console.log(error);
  }
};

export default connect;
